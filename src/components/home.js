import { useEffect, useState } from "react"
import BlogList from "./BlogList"

const Home = () => {
    const [blogs, setBlogs] = useState(null)

    const handleDelete = (id) => {
        console.log("kepencet")
        const filterBlogs = blogs.filter(blog => blog.id !== id)
        setBlogs(filterBlogs)
    }

    useEffect(() => {
        fetch("http://localhost:8000/blogs") 
            .then((res => {
                console.log(res)
                return res.json()
            }))       
            .then((data) => {
                console.log(data)
                setBlogs(data)
            })
    }, [])
    return(
        <div className="home">
            <BlogList blogs = {blogs} title = {"All Blogs"} handleDelete = {handleDelete}  />
        </div>
    )
}

export default Home